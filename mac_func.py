from uuid import getnode as get_mac
from subprocess import Popen, PIPE
import re

from conversion import *

def get_mac_from_ip(ip, repeat=10):
    '''
    Get MAC address from IP address.
    This has chance to fail even after pinging, so repeat for at most repeat times.
    returns None if not found.

    It's kinda cheating 'cause arp is called...?
    '''
    ip=arr_to_ip(ip)
    cnt=0
    while cnt<repeat:
        Popen(['ping', '-c 1', ip], stdout=PIPE)
        try:
            pid=Popen(['arp', '-n', ip], stdout=PIPE)
            s=pid.communicate()[0].decode()
            mac=re.search(r"(([a-f\d]{1,2}\:){5}[a-f\d]{1,2})",s).groups()[0]
            return mac_to_arr(mac)
        except:
            #ARP didn't find the MAC; ping again
            cnt+=1

def get_local_mac():
    '''
    Get local MAC address.
    '''
    mac=get_mac()
    arr=[]
    for _ in range(6):
        arr.append(mac&0xff)
        mac>>=8
    return [*reversed(arr)]