def mac_to_arr(mac):
	'''
	input: MAC address in string('aa:bb:cc:dd:ee:ff'')
	output: array form of said MAC address([0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff])
	'''
	return [*map(lambda x:int(x,16), mac.split(':'))]

def arr_to_mac(arr):
	'''
	input: MAC address in array form([0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff])
	output: string of said MAC address(aa:bb:cc:dd:ee:ff)
	'''
	return ':'.join(map(lambda x:hex(x)[2:], arr))

def ip_to_arr(ip):
	'''
	input: IP address in string('127.0.0.1')
	output: array form of said IP address([127, 0, 0, 1])
	'''
	return [*map(int, ip.split('.'))]

def arr_to_ip(arr):
	'''
	input: IP address in array form([127, 0, 0, 1])
	output: string of said IP address('127.0.0.1')
	'''
	return '.'.join(map(str, arr))