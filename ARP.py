import socket
from struct import pack, unpack

class ARP:
    '''
    Class for ARP packet. self.ARP_FRAME will use to pack and unpack data from the packets.
    The explanation of ARP_FRAME is as follows:

    !   : Network byte order
    6B  : Destination MAC
    6B  : Source MAC
    H   : Ethertype(ETY)
    H   : Hardware Type(HRD)
    H   : Protocol Type(PRO)
    B   : Hardware Addr len(HLN)
    B   : Protocol Addr len(PLN)
    H   : Opcode(1 for request, 2 for response)
    6B  : Sender MAC addr
    4B  : Sender IP addr
    6B  : Target MAC addr
    4B  : Target IP addr
    '''
    ETY=0x0806
    HRD=0x0001
    PRO=0x0800
    HLN=0x06
    PLN=0x04
    ARP_FRAME='!6B6BHHHBBH6B4B6B4B'

    ARP_REQUEST=0x0001
    ARP_REPLY=0x0002

    @classmethod
    def startup(cls, interface):
        '''
        Startup of this class. When this classmethod is called, there is no need to call
        __init__, as this will create socket for the user.
        '''
        arp=ARP()
        arp.create_socket(interface)
        return arp

    def create_socket(self, interface):
        '''
        Creates the socket and binds it to the interface given.
        '''
        self.socket=socket.socket(socket.AF_PACKET, socket.SOCK_RAW, socket.htons(0x0806))
        self.socket.bind((interface, 0))

    def send_arp_packet(self, *, opcode, sender_mac, sender_ip, target_mac, target_ip):
        '''
        Send an ARP packet using the provided variables.
        '''
        packet=pack(self.ARP_FRAME,
                    *target_mac, *sender_mac, self.ETY,
                    self.HRD, self.PRO, self.HLN, self.PLN, opcode,
                    *sender_mac, *sender_ip, *target_mac, *target_ip)
        self.socket.send(packet)
